# Préparation à la certification LPIC-1 : SQL

**Bâtir et manipuler la base de données SQL décrite dans le livre de Sébastien Rohaut *Linux - Préparation à la certification LPIC-1*, 5e édition, mai 2017, Paris:éditions Eni, pp. 277-282 et pp.819-820.** Voir [ici](https://www.editions-eni.fr/livre/linux-preparation-a-la-certification-lpic-1-examens-lpi-101-et-lpi-102-5e-edition-9782409007903).

La base de données est décrite à la fin du livre et ses manipulations le sont dans un chapitre dédié mais, selon votre version de MySQL, certaines commandes peuvent ne pas parfaitement fonctionner. En appui du contenu du livre, voici un tuto pas à pas pour réaliser et manipuler cette base de données avec la dernière version de MariaDB (15.1).

La base ainsi construite est jointe à ce tuto (attention il s'agit de la base modifiée, une fois toutes les manipulations du tuto réalisées). Pour l'importer, créer une base vide nommée "lpic" (voir ci-après), puis : `sudo mysql -u root -p lpic < lpic_2019-05-14_104756.sql`

En complément : [Pense-bête MySQL \[en\]](https://gitlab.com/sakura-lain/mysql-reminder)

*Nota:* La casse n'a pas d'importance pour l'interprétation des commandes, mais par convention on note ces dernières en majuscules. De plus, toutes les commandes situées entre le début du prompt et sa fin (figurée par un point-virgule à ne pas oublier) peuvent être écrites sur une seule ligne.

## Création de la base de donnée et de ses tables

### Créer la base de données : CREATE DATABASE

```
MariaDB [(none)]> CREATE DATABASE lpic;
Query OK, 1 row affected (0.00 sec)
```

### Visualiser les bases de données : SHOW DATABASES

```
MariaDB [(none)]> SHOW DATABASES
    -> ;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| lpic               |
| mysql              |
| performance_schema |
| sakila             |
| test               |
+--------------------+
6 rows in set (0.13 sec)

MariaDB [(none)]> USE lpic
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
```

### Sélectionner la base de données à utiliser : USE

```
MariaDB [(none)]> USE lpic
Database changed
MariaDB [lpic]>
```

### Créer les tables : CREATE TABLE

```
MariaDB [lpic]> CREATE TABLE IF NOT EXISTS `t_commande` (
    -> id_commande INT NOT NULL AUTO_INCREMENT,
    -> id INT NOT NULL,
    -> id_produit INT NOT NULL,
    -> qte INT NOT NULL,
    -> PRIMARY KEY (`id_commande`)
    -> )
    -> ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;
Query OK, 0 rows affected (0.09 sec)

MariaDB [lpic]> INSERT INTO `t_commande`(`id_commande`, `id`, `id_produit`, `qte`) VALUES
    -> (1, 1, 3, 1),
    -> (2, 1, 1, 2),
    -> (3, 2, 2, 5);
Query OK, 3 rows affected (0.03 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [lpic]> CREATE TABLE IF NOT EXISTS t_fournisseurs (
    -> id_fournisseur INT(11) NOT NULL AUTO_INCREMENT,
    -> `nom` VARCHAR(255) NOT NULL,
    -> adresse VARCHAR(255) NOT NULL,
    -> cp VARCHAR(5) NOT NULL,
    -> ville VARCHAR(100) NOT NULL,
    -> tel VARCHAR(10) NOT NULL,
    -> PRIMARY KEY (id_fournisseur)
    -> )
    -> ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=3;
Query OK, 0 rows affected (0.06 sec)

MariaDB [lpic]> INSERT INTO t_fournisseurs (id_fournisseur, nom, adresse, cp, ville, tel) VALUES 
    -> (1, 'Espace Multimedia', '48 rue des oiseaux', '77120', 'Poissy sur Marne', '0160606161'), 
    -> (2, 'les légumes associés', '14 avenue des maraichers', '28140', 'Andouille sur Vire', '0321414141');
Query OK, 2 rows affected (0.00 sec)
Records: 2  Duplicates: 0  Warnings: 0

MariaDB [lpic]> CREATE TABLE IF NOT EXISTS t_produits ( 
    -> id_produit INT(11) NOT NULL AUTO_INCREMENT, 
    -> id_fournisseur INT(11) NOT NULL, 
    -> nom VARCHAR(150) NOT NULL, 
    -> prix FLOAT NOT NULL, 
    -> tva FLOAT NOT NULL, 
    -> qte INT(11) NOT NULL, 
    -> PRIMARY KEY (id_produit), 
    -> KEY id_fournisseur (id_fournisseur) 
    -> ) 
    -> ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=4;
Query OK, 0 rows affected (0.11 sec)

MariaDB [lpic]> INSERT INTO t_produits (id_produit, id_fournisseur, nom, prix, tva, qte) VALUES
    -> (1, 1, 'Clé USB 1 Go', 10.71, 1.196, 20),
    -> (2, 1, 'Ananas premier choix', 16.002, 1.196, 30),
    -> (3, 2, 'Chateau Lapompe 2009', 14.9835, 1.196, 25);
Query OK, 3 rows affected (0.00 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [lpic]> CREATE TABLE IF NOT EXISTS t_utilisateurs (
    -> id INT(11) NOT NULL AUTO_INCREMENT, 
    -> nom VARCHAR(100) NOT NULL, 
    -> prenom VARCHAR(100) NOT NULL, 
    -> ville VARCHAR(100) NOT NULL, 
    -> id_parrain INT(11) DEFAULT NULL,
    -> PRIMARY KEY (id),
    -> KEY id_parrain (id_parrain)
    -> )
    -> ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=4;
Query OK, 0 rows affected (0.07 sec)

MariaDB [lpic]> INSERT INTO t_utilisateurs (id, nom, prenom, ville, id_parrain) VALUES
    -> (1, 'Robert', 'Jacques', 'Paris', NULL),
    -> (2, 'Rousseau', 'François', 'Saint Marcelin les Clos', 1);
Query OK, 2 rows affected (0.00 sec)
Records: 2  Duplicates: 0  Warnings: 0
``` 

### Visualiser les tables : SHOW TABLES

```
MariaDB [lpic]> SHOW tables
    -> ;
+----------------+
| Tables_in_lpic |
+----------------+
| t_commande     |
| t_fournisseurs |
| t_produits     |
| t_utilisateurs |
+----------------+
4 rows in set (0.00 sec)
```

## Les requêtes de sélection

### Sélectionner : SELECT (FROM)

```
MariaDB [lpic]> SELECT nom, prenom, id AS identifiant FROM t_utilisateurs;
+----------+-----------+-------------+
| nom      | prenom    | identifiant |
+----------+-----------+-------------+
| Robert   | Jacques   |           1 |
| Rousseau | François  |           2 |
+----------+-----------+-------------+
2 rows in set (0.00 sec)

MariaDB [lpic]> SELECT * FROM t_utilisateurs;
+----+----------+-----------+-------------------------+------------+
| id | nom      | prenom    | ville                   | id_parrain |
+----+----------+-----------+-------------------------+------------+
|  1 | Robert   | Jacques   | Paris                   |       NULL |
|  2 | Rousseau | François  | Saint Marcelin les Clos |          1 |
+----+----------+-----------+-------------------------+------------+
2 rows in set (0.00 sec)
```

## Sélectionner en excluant les doublons : SELECT DISTINCT

```
MariaDB [lpic]> SELECT DISTINCT prenom FROM t_utilisateurs;
+-----------+
| prenom    |
+-----------+
| Jacques   |
| François  |
+-----------+
2 rows in set (0.00 sec)

MariaDB [lpic]> SELECT DISTINCT t_fournisseurs.nom 
    -> FROM t_fournisseurs, t_produits 
    -> WHERE t_produits.id_fournisseur=t_fournisseurs.id_fournisseur;
+------------------------+
| nom                    |
+------------------------+
| Espace Multimedia      |
| les légumes associés   |
+------------------------+
2 rows in set (0.29 sec)

MariaDB [lpic]> SELECT DISTINCT nom 
    -> FROM t_fournisseurs;
+------------------------+
| nom                    |
+------------------------+
| Espace Multimedia      |
| les légumes associés   |
+------------------------+
2 rows in set (0.00 sec)
```
**À comparer avec le contenu complet des tables :**

```
MariaDB [lpic]> SELECT * 
    -> FROM t_fournisseurs;
+----------------+------------------------+--------------------------+-------+--------------------+------------+
| id_fournisseur | nom                    | adresse                  | cp    | ville              | tel        |
+----------------+------------------------+--------------------------+-------+--------------------+------------+
|              1 | Espace Multimedia      | 48 rue des oiseaux       | 77120 | Poissy sur Marne   | 0160606161 |
|              2 | les légumes associés   | 14 avenue des maraichers | 28140 | Andouille sur Vire | 0321414141 |
+----------------+------------------------+--------------------------+-------+--------------------+------------+
2 rows in set (0.00 sec)

MariaDB [lpic]> SELECT * 
    -> FROM t_produits;
+------------+----------------+----------------------+---------+-------+-----+
| id_produit | id_fournisseur | nom                  | prix    | tva   | qte |
+------------+----------------+----------------------+---------+-------+-----+
|          1 |              1 | Clé USB 1 Go         |   10.71 | 1.196 |  20 |
|          2 |              1 | Ananas premier choix |  16.002 | 1.196 |  30 |
|          3 |              2 | Chateau Lapompe 2009 | 14.9835 | 1.196 |  25 |
+------------+----------------+----------------------+---------+-------+-----+
3 rows in set (0.04 sec)
```

### Sélectionner avec des conditions : WHERE (attributs : BETWEEN, LIKE, IN)

**Sélectionner en prenant pour référence un champ commun à deux tables (voir la jointure ci-après):**

```
MariaDB [lpic]> SELECT DISTINCT t_fournisseurs.nom 
    -> FROM t_fournisseurs, t_produits 
    -> WHERE t_produits.id_fournisseur=t_fournisseurs.id_fournisseur;
+------------------------+
| nom                    |
+------------------------+
| Espace Multimedia      |
| les légumes associés   |
+------------------------+
2 rows in set (0.00 sec)
```

**Selectionner une chaîne de caractères :**

```
MariaDB [lpic]> SELECT prenom 
    -> FROM t_utilisateurs 
    -> WHERE nom='ROHAUT';
Empty set (0.00 sec)

MariaDB [lpic]> SELECT prenom 
    -> FROM t_utilisateurs 
    -> WHERE nom='ROUSSEAU';
+-----------+
| prenom    |
+-----------+
| François  |
+-----------+
1 row in set (0.00 sec)
```

**`=`, `>=`, `<=`, `>`, `<`, `<>`, `!=`, `!>`, `!<`, `AND`, `OR`, `NOT`, etc. :**

```
MariaDB [lpic]> SELECT DISTINCT t_fournisseurs.nom 
    -> FROM t_fournisseurs, t_utilisateurs, t_commande, t_produits 
    -> WHERE t_utilisateurs.id=t_commande.id 
    -> AND t_commande.id_produit=t_produits.id_produit 
    -> AND t_produits.id_fournisseur=t_fournisseurs.id_fournisseur 
    -> AND t_utilisateurs.id='1';
+------------------------+
| nom                    |
+------------------------+
| Espace Multimedia      |
| les légumes associés   |
+------------------------+
2 rows in set (0.07 sec)

MariaDB [lpic]> SELECT B.nom, B.prenom
    -> FROM t_utilisateurs AS A, t_utilisateurs AS B
    -> WHERE B.id = A.id_parrain
    -> AND A.id = '2';
+--------+---------+
| nom    | prenom  |
+--------+---------+
| Robert | Jacques |
+--------+---------+
1 row in set (0.04 sec)
```

**`BETWEEN` nous permet de sélectionner les résultats dont l'identifiant (champ "id") est compris entre 0 et 100 :**

```
MariaDB [lpic]> SELECT * 
    -> FROM t_utilisateurs 
    -> WHERE id BETWEEN 0 AND 100;
+----+----------+-----------+-------------------------+------------+
| id | nom      | prenom    | ville                   | id_parrain |
+----+----------+-----------+-------------------------+------------+
|  1 | Robert   | Jacques   | Paris                   |       NULL |
|  2 | Rousseau | François  | Saint Marcelin les Clos |          1 |
+----+----------+-----------+-------------------------+------------+
2 rows in set (0.01 sec)
```
**`LIKE` compare la valeur d'un champ avec une chaîne de caratères éventuellement accompagnée de caratères de remplacement ("%" pour une chaîne quelconque et "_" pour un caractère quelconque):**

```
MariaDB [lpic]> SELECT * 
    -> FROM t_utilisateurs 
    -> WHERE prenom LIKE 'Jean-%';
Empty set (0.00 sec)

MariaDB [lpic]> SELECT * 
    -> FROM t_utilisateurs 
    -> WHERE prenom LIKE 'Jac%';
+----+--------+---------+-------+------------+
| id | nom    | prenom  | ville | id_parrain |
+----+--------+---------+-------+------------+
|  1 | Robert | Jacques | Paris |       NULL |
+----+--------+---------+-------+------------+
1 row in set (0.00 sec)
```

**`IN` propose une liste :**

```
MariaDB [lpic]> SELECT * 
    -> FROM t_utilisateurs 
    -> WHERE ville IN ('Paris', 'Lille', 'Lyon', 'Marseille');
+----+--------+---------+-------+------------+
| id | nom    | prenom  | ville | id_parrain |
+----+--------+---------+-------+------------+
|  1 | Robert | Jacques | Paris |       NULL |
+----+--------+---------+-------+------------+
1 row in set (0.00 sec)
```

## Les expressions et les fonctions

### Ajouter des calculs à vos sélections

**Additionner et multiplier :**

```
MariaDB [lpic]> SELECT prix+(prix*tva) AS prix_net 
    -> FROM t_produits 
    -> WHERE id_produit='1245';
Empty set (0.00 sec)

MariaDB [lpic]> SELECT prix+(prix*tva) AS prix_net 
    -> FROM t_produits 
    -> WHERE id_produit='1';
+-------------------+
| prix_net          |
+-------------------+
| 23.51915986927986 |
+-------------------+
1 row in set (0.00 sec)

MariaDB [lpic]> SELECT nom+' '+prenom 
    -> FROM t_utilisateurs;
+----------------+
| nom+' '+prenom |
+----------------+
|              0 |
|              0 |
+----------------+
2 rows in set, 6 warnings (0.00 sec)

MariaDB [lpic]> SELECT nom+prenom 
    -> FROM t_utilisateurs;
+------------+
| nom+prenom |
+------------+
|          0 |
|          0 |
+------------+
2 rows in set, 4 warnings (0.00 sec)
```
**Compter le nombre de lignes dans une table :**

```
MariaDB [lpic]> SELECT COUNT(*) 
    -> FROM t_utilisateurs;
+----------+
| COUNT(*) |
+----------+
|        2 |
+----------+
1 row in set (0.03 sec)
```

**Valeurs minimales, maximales et moyenne :**

```
MariaDB [lpic]> SELECT min(prix), max(prix), avg(prix) 
    -> FROM t_produits;
+--------------------+-------------------+--------------------+
| min(prix)          | max(prix)         | avg(prix)          |
+--------------------+-------------------+--------------------+
| 10.710000038146973 | 16.00200080871582 | 13.898500124613443 |
+--------------------+-------------------+--------------------+
1 row in set (0.00 sec)
```

**Somme : on extrait le montant total du stock de produits :**

```
MariaDB [lpic]> SELECT sum(prix*qte) 
    -> FROM t_produits;
+--------------------+
| sum(prix*qte)      |
+--------------------+
| 1068.8475131988525 |
+--------------------+
1 row in set (0.00 sec)
```

**`current_date` retourne la date du jour :**

```
MariaDB [lpic]> SELECT current_date;
+--------------+
| current_date |
+--------------+
| 2019-02-26   |
+--------------+
1 row in set (0.00 sec)
```

**Les autres fonctions de date, comme `WHERE MONTH`, s'appliquent sur les champs de type date.**

### Ajouter des fonctions à vos sélections : RIGHT et LEFT (lire depuis la gauche ou la droite)
 
```
MariaDB [lpic]> SELECT * 
    -> FROM t_utilisateurs 
    -> WHERE LEFT(nom,2) 
    -> LIKE 'RI';
Empty set (0.00 sec)

MariaDB [lpic]> SELECT * 
    -> FROM t_utilisateurs 
    -> WHERE LEFT(nom,2) 
    -> LIKE 'RO';
+----+----------+-----------+-------------------------+------------+
| id | nom      | prenom    | ville                   | id_parrain |
+----+----------+-----------+-------------------------+------------+
|  1 | Robert   | Jacques   | Paris                   |       NULL |
|  2 | Rousseau | François  | Saint Marcelin les Clos |          1 |
+----+----------+-----------+-------------------------+------------+
2 rows in set (0.00 sec)

MariaDB [lpic]> SELECT * 
    -> FROM t_utilisateurs 
    -> WHERE RIGHT(nom,2) 
    -> LIKE 'AU';
+----+----------+-----------+-------------------------+------------+
| id | nom      | prenom    | ville                   | id_parrain |
+----+----------+-----------+-------------------------+------------+
|  2 | Rousseau | François  | Saint Marcelin les Clos |          1 |
+----+----------+-----------+-------------------------+------------+
1 row in set (0.01 sec)
```

## Trier les résultats : ORDER BY (ASC: ascendant, DESC, décroissant)

```
MariaDB [lpic]> SELECT prix+(prix*tva) AS prix_net 
    -> FROM t_produits 
    -> ORDER BY prix_net ASC;
+--------------------+
| prix_net           |
+--------------------+
|  23.51915986927986 |
| 32.903764661165724 |
|   35.1403934554653 |
+--------------------+
3 rows in set (0.09 sec)

MariaDB [lpic]> SELECT prix+(prix*tva) AS prix_net
    -> FROM t_produits
    -> ORDER BY prix_net DESC;
+--------------------+
| prix_net           |
+--------------------+
|  36.89740956798164 |
|  34.54900964890794 |
| 24.695116920322903 |
+--------------------+
3 rows in set (0.00 sec)
```

## Regrouper les résultats par champs de valeurs identiques : GROUP BY (HAVING)

```
MariaDB [lpic]> SELECT id_fournisseur, sum(qte) 
    -> FROM t_produits 
    -> GROUP BY id_fournisseur;
+----------------+----------+
| id_fournisseur | sum(qte) |
+----------------+----------+
|              1 |       50 |
|              2 |       25 |
+----------------+----------+
2 rows in set (0.04 sec)

MariaDB [lpic]> SELECT id_fournisseur, sum(qte) AS quantité 
    -> FROM t_produits 
    -> GROUP BY id_fournisseur;
+----------------+-----------+
| id_fournisseur | quantité  |
+----------------+-----------+
|              1 |        50 |
|              2 |        25 |
+----------------+-----------+
2 rows in set (0.00 sec)

MariaDB [lpic]> SELECT id_fournisseur, sum(qte) AS quantité 
    -> FROM t_produits 
    -> GROUP BY id_fournisseur HAVING id_fournisseur='1';
+----------------+-----------+
| id_fournisseur | quantité  |
+----------------+-----------+
|              1 |        50 |
+----------------+-----------+
1 row in set (0.00 sec)

``` 

## Joindre plusieurs tables entre elles : INNER JOIN (ON)

**On part de ces deux tables :**

```
MariaDB [lpic]> SELECT * 
    -> FROM t_fournisseurs;
+----------------+------------------------+--------------------------+-------+--------------------+------------+
| id_fournisseur | nom                    | adresse                  | cp    | ville              | tel        |
+----------------+------------------------+--------------------------+-------+--------------------+------------+
|              1 | Espace Multimedia      | 48 rue des oiseaux       | 77120 | Poissy sur Marne   | 0160606161 |
|              2 | les légumes associés   | 14 avenue des maraichers | 28140 | Andouille sur Vire | 0321414141 |
+----------------+------------------------+--------------------------+-------+--------------------+------------+
2 rows in set (0.00 sec)

MariaDB [lpic]> SELECT * 
    -> FROM t_produits ;
+------------+----------------+----------------------+---------+-------+-----+
| id_produit | id_fournisseur | nom                  | prix    | tva   | qte |
+------------+----------------+----------------------+---------+-------+-----+
|          1 |              1 | Clé USB 1 Go         | 11.2455 | 1.196 |  20 |
|          2 |              1 | Ananas premier choix | 16.8021 | 1.196 |  30 |
|          3 |              2 | Chateau Lapompe 2009 | 15.7327 | 1.196 |  25 |
+------------+----------------+----------------------+---------+-------+-----+
3 rows in set (0.00 sec)
```
**Un exemple de jointure sans INNER JOIN :**

```
MariaDB [lpic]> SELECT DISTINCT t_fournisseurs.nom 
    -> FROM t_fournisseurs, t_produits 
    -> WHERE t_produits.id_fournisseur=t_fournisseurs.id_fournisseur;
+------------------------+
| nom                    |
+------------------------+
| Espace Multimedia      |
| les légumes associés   |
+------------------------+
2 rows in set (0.00 sec)

```
**Avec INNER JOIN :**

```
MariaDB [lpic]> SELECT *
    -> FROM t_fournisseurs
    -> INNER JOIN t_produits 
    -> ON t_fournisseurs.id_fournisseur=t_produits.id_fournisseur;
+----------------+------------------------+--------------------------+-------+--------------------+------------+------------+----------------+----------------------+---------+-------+-----+
| id_fournisseur | nom                    | adresse                  | cp    | ville              | tel        | id_produit | id_fournisseur | nom                  | prix    | tva   | qte |
+----------------+------------------------+--------------------------+-------+--------------------+------------+------------+----------------+----------------------+---------+-------+-----+
|              1 | Espace Multimedia      | 48 rue des oiseaux       | 77120 | Poissy sur Marne   | 0160606161 |          1 |              1 | Clé USB 1 Go         | 11.2455 | 1.196 |  20 |
|              1 | Espace Multimedia      | 48 rue des oiseaux       | 77120 | Poissy sur Marne   | 0160606161 |          2 |              1 | Ananas premier choix | 16.8021 | 1.196 |  30 |
|              2 | les légumes associés   | 14 avenue des maraichers | 28140 | Andouille sur Vire | 0321414141 |          3 |              2 | Chateau Lapompe 2009 | 15.7327 | 1.196 |  25 |
+----------------+------------------------+--------------------------+-------+--------------------+------------+------------+----------------+----------------------+---------+-------+-----+
3 rows in set (0.03 sec)
```

[**Plus sur INNER JOIN**](https://sql.sh/cours/jointures/inner-join)

### Renommer les tables dans la requête : AS

```
MariaDB [lpic]> SELECT DISTINCT t1.nom
    -> FROM t_fournisseurs AS t1
    -> INNER JOIN t_produits AS t2
    -> ON t1.id_fournisseur=t2.id_fournisseur;
+------------------------+
| nom                    |
+------------------------+
| Espace Multimedia      |
| les légumes associés   |
+------------------------+
2 rows in set (0.00 sec)
```

**C'est l'exact équivalent de :**

```
MariaDB [lpic]> SELECT DISTINCT t_fournisseurs.nom
    -> FROM t_fournisseurs
    -> INNER JOIN t_produits
    -> ON t_fournisseurs.id_fournisseur=t_produits.id_fournisseur;
+------------------------+
| nom                    |
+------------------------+
| Espace Multimedia      |
| les légumes associés   |
+------------------------+
2 rows in set (0.00 sec)
```

## Un SELECT dans un SELECT

```
MariaDB [lpic]> SELECT id
    -> FROM t_utilisateurs
    -> WHERE id 
    -> IN (SELECT id_parrain FROM t_utilisateurs WHERE id_parrain!='');
+----+
| id |
+----+
|  1 |
+----+
1 row in set, 3 warnings (0.00 sec)

MariaDB [lpic]> SELECT id FROM t_utilisateurs 
    -> WHERE id 
    -> IN (SELECT id_parrain FROM t_utilisateurs WHERE id_parrain='');
Empty set (0.00 sec)

MariaDB [lpic]> SELECT id FROM t_utilisateurs 
    -> WHERE id 
    -> IN (SELECT id_parrain FROM t_utilisateurs WHERE id_parrain IN ('1', '2'));
+----+
| id |
+----+
|  1 |
+----+
1 row in set (0.03 sec)

```

## Insérer : INSERT INTO

```
MariaDB [lpic]> INSERT INTO t_utilisateurs (id, nom, prenom, ville, id_parrain)
    -> VALUES ('3', 'Le Canet', 'Jules', 'Beauvais', NULL);
Query OK, 1 row affected (0.10 sec)

MariaDB [lpic]> SELECT * 
    ->FROM t_utilisateurs;
+----+----------+-----------+-------------------------+------------+
| id | nom      | prenom    | ville                   | id_parrain |
+----+----------+-----------+-------------------------+------------+
|  1 | Robert   | Jacques   | Paris                   |       NULL |
|  2 | Rousseau | François  | Saint Marcelin les Clos |          1 |
|  3 | Le Canet | Jules     | Beauvais                |       NULL |
+----+----------+-----------+-------------------------+------------+
3 rows in set (0.00 sec)
```

## Modifier : UPDATE

```
MariaDB [lpic]> SELECT * 
    ->FROM t_produits;
+------------+----------------+----------------------+---------+-------+-----+
| id_produit | id_fournisseur | nom                  | prix    | tva   | qte |
+------------+----------------+----------------------+---------+-------+-----+
|          1 |              1 | Clé USB 1 Go         |   10.71 | 1.196 |  20 |
|          2 |              1 | Ananas premier choix |  16.002 | 1.196 |  30 |
|          3 |              2 | Chateau Lapompe 2009 | 14.9835 | 1.196 |  25 |
+------------+----------------+----------------------+---------+-------+-----+
3 rows in set (0.00 sec)

MariaDB [lpic]> UPDATE t_produits 
    -> SET prix=prix*1.05;
Query OK, 3 rows affected (0.97 sec)
Rows matched: 3  Changed: 3  Warnings: 0

MariaDB [lpic]> SELECT * 
    -> FROM t_produits;
+------------+----------------+----------------------+---------+-------+-----+
| id_produit | id_fournisseur | nom                  | prix    | tva   | qte |
+------------+----------------+----------------------+---------+-------+-----+
|          1 |              1 | Clé USB 1 Go         | 11.2455 | 1.196 |  20 |
|          2 |              1 | Ananas premier choix | 16.8021 | 1.196 |  30 |
|          3 |              2 | Chateau Lapompe 2009 | 15.7327 | 1.196 |  25 |
+------------+----------------+----------------------+---------+-------+-----+
3 rows in set (0.07 sec)
```

## Supprimer : DELETE

```
MariaDB [lpic]> DELETE 
    -> FROM t_utilisateurs
    -> WHERE id=3;
Query OK, 1 row affected (0.09 sec)

MariaDB [lpic]> SELECT * 
    -> FROM t_utilisateurs;
+----+----------+-----------+-------------------------+------------+
| id | nom      | prenom    | ville                   | id_parrain |
+----+----------+-----------+-------------------------+------------+
|  1 | Robert   | Jacques   | Paris                   |       NULL |
|  2 | Rousseau | François  | Saint Marcelin les Clos |          1 |
+----+----------+-----------+-------------------------+------------+
2 rows in set (0.00 sec)
